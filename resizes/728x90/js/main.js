"use strict";
~(function() {
    var count = 0,
        bgExit = document.getElementById("bgExit");

    window.init = function() {
        var tl = new TimelineMax({
            onComplete: doLoop
        });
        tl.set("#mainContent", { force3D: true });

        tl.addLabel("frameOne")
        tl.to("#copyOne", 0.5, { y: 0, rotation: 0.01, ease: Power1.easeOut }, "frameOne");

        tl.addLabel("frameTwo")
        tl.to("#bg2", 0.5, { opacity: 1, ease: Power1.easeOut }, "frameTwo+=2.6");
        tl.to("#copyTwo", 0.5, { y: 0, rotation: 0.01, ease: Power1.easeOut }, "frameTwo+=3");

        tl.addLabel("frameThree")
        tl.to("#bg3", 0.5, { opacity: 1, ease: Power1.easeOut }, "frameThree+=2.6");
        tl.to("#copyThree", 0.5, { y: 0, rotation: 0.01, ease: Power1.easeOut }, "frameThree+=3");

        tl.addLabel("frameFour")
        tl.to("#bg4", 0.5, { opacity: 1, ease: Power1.easeOut }, "frameFour+=2.6");
        tl.to("#copyFour", 0.5, { y: 0, rotation: 0.01, ease: Power1.easeOut }, "frameFour+=3", '+=0.5');
        tl.to("#ctaContainer", 0.5, { x: 0, rotation: 0.01, ease: Power1.easeOut }, "frameFour+=4.3");
        tl.to("#ctaContainer", 0.5, { scale: 1.1, yoyo: true, repeat: 1, force3D: false, rotation: 0.01, ease: Power1.easeInOut }, "frameFour+=5.4")

    };



    function doLoop() {
        count++;
        if (count < 3) {

            var tlLoop = new TimelineMax();

            tlLoop.to(["#bg2", "#bg3", "#bg4"], 0, { opacity: 0, ease: Power0.easeNone }, .5)
            tlLoop.set(["#mainContent *"], { clearProps: 'all' }, .5)
            tlLoop.add(init, 1);
        }

    }

    bgExit.addEventListener("click", function(e) {
        e.preventDefault();
        window.open(window.clickTag);
    });

})();
window.onload = init();