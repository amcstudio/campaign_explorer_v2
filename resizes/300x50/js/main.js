"use strict";
~(function () {
  var
    count = 0,
    bgExit = document.getElementById("bgExit");

  window.init = function () {
    var tl = new TimelineMax({
      onComplete: doLoop
    });
    tl.set("#mainContent", {force3D: true });
   
    tl.to(".blueLogo", 0.5, { opacity: 0, ease: Power1.easeOut },"+=1.5");

    tl.to("#bg1", 0.5, { opacity:1, ease: Power1.easeOut },"-=0.5"); 
    tl.to("#copyOne", 0.5, { y: 0, ease: Power1.easeOut });

    tl.to("#bg2", 0.5, { opacity:1, ease: Power1.easeOut },"+=2");
    tl.to("#copyTwo", 0.5, { y: 0, ease: Power1.easeOut });

    tl.to("#bg3", 0.5, { opacity:1, ease: Power1.easeOut },"+=2.5");
    tl.to("#copyThree", 0.5, { y: 0, ease: Power1.easeOut });

    tl.to("#bg4", 0.5, { opacity:1, ease: Power1.easeOut },"+=2");
    tl.to("#whiteLogo", 0.5, { y: 0, ease: Power1.easeOut });
    tl.to(["#copyFour","#cta"], 0.5, { y: 0, ease: Power1.easeOut },"-=0.5");
    tl.to("#cta", 0.5, {scale: 1.1, yoyo: true, repeat: 1, force3D: false, rotation: 0.01, ease: Power1.easeInOut },"+=0.5");
  };

 

  function doLoop() {
      count++;
      if (count < 3) {

        var tlLoop = new TimelineMax();

        tlLoop.to(["#copyFour","#cta","#whiteLogo"], 0.25, { opacity: 0, ease: Power0.easeNone },1)
        tlLoop.set(["#mainContent *"], { clearProps: 'all' })
        tlLoop.add(init, 1.5);
      }

  }

  bgExit.addEventListener("click", function (e) {
    e.preventDefault();
    window.open(window.clickTag);
  });
  
})();

window.onload = init();