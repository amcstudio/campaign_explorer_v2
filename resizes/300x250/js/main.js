"use strict";
~(function () {
  var
    count = 0,
    bgExit = document.getElementById("bgExit");

  window.init = function () {
    var tl = new TimelineMax({
      onComplete: doLoop
    });
    tl.set("#mainContent", {force3D: true });
   
    tl.to("#blueCopyOne", 0.5, { x: 0, ease: Power1.easeOut });
    tl.to("#copyOne", 0.5, { x: 0, ease: Power1.easeOut });
    tl.to(["#copyOne","#blueCopyOne"], 0.5, { opacity: 0, ease: Power1.easeOut }, "+=2.4");

    tl.to("#parisBg", 0.5, { opacity:1, rotation: 0.01, ease: Sine.easeOut },"-=0.5");
    tl.to("#londonBg", 0.5, { opacity:0, rotation: 0.01, ease: Sine.easeInOut },"-=0.5");
    tl.to("#blueCopyTwo", 0.5, { x: 0, ease: Power1.easeOut },"-=0.5");
    tl.to("#copyTwo", 0.5, { x: 0, ease: Power1.easeOut });
    tl.to(["#copyTwo", "#blueCopyTwo"],0.5, {opacity:0, ease:Power1.easeOut},"+=2.7");

    tl.to("#frankfurtBg", 0.5, { opacity:1, rotation: 0.01, ease: Sine.easeOut },"-=0.5");
    tl.to("#parisBg", 0.5, { opacity:0, rotation: 0.01, ease: Sine.easeInOut },"-=0.5");
    tl.to("#blueCopyThree", 0.5, { x: 0, ease: Power1.easeOut }, "-=0.5");
    tl.to("#copyThree", 0.5, { x:0, ease:Power1.easeOut});
    tl.to(["#copyThree",".blueSmall"] ,0.5, {opacity:0, ease:Power1.easeOut},"+=2.4");

    tl.to("#endFrameBg", 0.5, { opacity:1, rotation: 0.01, ease: Sine.easeOut },"-=0.5");
    tl.to("#frankfurtBg", 0.5, { opacity:0, rotation: 0.01, ease: Sine.easeInOut },"-=0.5");
    tl.to("#blueCopyFour", 0.5, { x: 0, ease: Power1.easeOut }, "-=0.5");
    tl.to("#copyFour", 0.5, { x: 0, ease: Power1.easeOut });
    tl.to("#ctaContainer",0.5, { opacity:1, ease:Power1.easeOut},"+=.5");
    tl.to("#ctaContainer", 0.5, {scale: 1.1, yoyo: true, repeat: 1, force3D: false, rotation: 0.01, ease: Power1.easeInOut },"+=.5");
  };



  function doLoop() {
    count++;
    if (count < 3) {

      var tlLoop = new TimelineMax();

      tlLoop.to(["#copyFour","#ctaContainer"], 0.25, { opacity: 0, ease: Power0.easeNone },0.5)
      tlLoop.set(["#mainContent *"], { clearProps: 'all' })
      tlLoop.add(init, 1);
    }

  }

  bgExit.addEventListener("click", function (e) {
    e.preventDefault();
    window.open(window.clickTag);
  });
  
})();

window.onload = init();